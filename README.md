# LibRocket

A general-purpose rocketry library for Kerbal Space Program's
kOS programming mod.

It will eventually include several components for
launching, landing, docking, etc. Currently, though, it only
supports launches.

## LibLaunch

LibLaunch can control a rocket's ascent, currently running in
several stages:

* Speed up to 300m/s below 8km, to reduce drag
* Start the roll. For the remainder of the launch, the rocket will tilt
down as it gets higher, although the altidiudes between which the roll
happens is configuratble.
* Throttle the engines to maintain 60sec time to apoapsis,
until it reaches 50km
* Throttle the engines to maintain 30sec time to apoapsis,
until it reaches 80km
* If it reaches 80km (which it often doesn't with lower TWRs, although
this does not break the launch) it tilts over fully.

The efficiency of this seems pretty good. On a test rocket (modeled
after the Angara A5 launch vehicle) it took ~4050m/s to reach orbit,
which is pretty good.

### Usage
The library can be invoked by simply running librocket/liblaunch. However,
this gives you no control over the paramaters of the launch.

If you run it inside a script and pass in a lexicon as a paramater, you
can set properties of the launch.

The lexicon's possible values are:
* `alt_roll_start`: The altitude that the rocket starts rolling at. Value in meters. Default: 30000
* `alt_roll_end`: The altitude that the rocket is horizontal at. Value in meters. Default: 90000
* `throttle_min`: The minimum throttle. LibLaunch will not throttle below this. It is especially
useful with RealFuel's limited ignitions. Value 0-1. Default: 0
* `throttle_max`: The maximim throttle. If you have engines which overheat, use this. Value 0-1. Default: 1
* `launch_clamps`: Should LibLaunch stage twice to start the engines then release the launch clamps two
seconds later, or should it only stage once? Value: boolean. Default: true

If you change any of these, you must call update_props() for these to take effect.

You can also put delegates in as callbacks. These do not have to be refreshed with update_props:
* `throttle_toohigh`: The rocket wants to throttle down, but has already hit throttle_min. This can be used, for example,
to turn off extra engines.
* `throttle_toolow`: The inverse of the above.
* `flameout`: One or more of the engines has flamed out. If this is not set, the rocket will automatically be staged at this point.

### Example
This example is for a simple two-stage rocket, with the first stage having both a central
and radial-mounted engines, and no launch clamps.

	LIST ENGINES IN engineList.
	LOCAL centralEngine TO engineList[1]. // Central engine on first stage, surrounded by radial mount engines

	LOCAL props IS lexicon().

	//SET props["alt_roll_start"] TO 30000.
	//SET props["alt_roll_end"] TO 90000.
	SET props["throttle_max"] TO 1.
	SET props["throttle_min"] TO 0.1.

	props["launch_clamps"] OFF. // Disable launch clamps

	SET props["throttle_toohigh"] TO {
		centralEngine:SHUTDOWN.
	}.
	SET props["throttle_toolow"] TO {
		centralEngine:ACTIVATE.
	}.

	// When the stage is done, disable all these callbacks.
	// Otherwise, the script will crash trying to turn on and off engines that have been staged away.
	SET props["flameout"] TO {
		STAGE.
		props:remove("throttle_toohigh").
		props:remove("throttle_toolow").
		props:remove("flameout").
	}.

	runpath("librocket/liblaunch", props).

This assumes you have downloaded this repository to 'librocket'.

## License and Credits
By ZNixian (or ZNix when username limits allow).

If you want to email me, find my email address in the git commits.

This project is hosted on [GitLab](https://gitlab.com/znixian/ksp-kos-librocket)

GNU LGPLv3 or later, or GNU GPLv3 or later.
